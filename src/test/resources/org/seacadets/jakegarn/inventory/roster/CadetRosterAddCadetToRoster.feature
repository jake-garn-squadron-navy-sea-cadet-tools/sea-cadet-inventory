Feature: Add cadets to Roster

    Scenario Outline: I have new cadet with first, last, middle initial, sex, 
      and usnscc Id and want to add them to the roster. USNSCC id are h
      exadecimal numbers which must be parsed from the id string. I wish to each
      one to the roster and increment the roster database by 1.

        Given New cadet "<first>", "<last>", "<middle>", "<sex>" and USNSCC ID "<usnsccId>"
        When I add a cadet to the roster
        Then I should be told the roster new size is <roster_size>

        Examples:
            |first  |last       |middle |sex    |usnsccId |roster_size |
            |Arthur |Klingenberg|R      |male   |A322A3F  |1           |
            |Dawson |Klingenberg|R      |male   |BF1233D  |2           |
            |Susan  |Russon     |J      |female |C32A111  |3           |

    
        Scenario Outline: I have new oficer/instructor with first, last, middle 
          initial, sex, and usnscc Id and want to add them to the roster.
          USNSCC id are hexadecimal numbers which must be parsed from the id
          string. I wish to each one to the roster and increment the roster 
          database by 1.

        Given New an officer with "<first>", "<last>", "<middle>", "sex" and USNSCC ID "<usnsccId>"
        When I add a officer/instructor to the roster
        Then I should be told the roster new size is <roster_size>

        Examples:
            |first  |last       |middle |sex    |usnsccId |roster_size |
            |Mark   |Smith      |B      |male   |D322A3A  |3           |
            |David  |Klingenberg|R      |male   |EF1233B  |4           |
            |Jim    |Russon     |A      |female |F32A11C  |5           |
            
            
        Scenario Outline: I have entered the same USNSCC ID that is already in 
          the roster.  I should see a exeption throw that telling me of the 
          duplicate entry.
          
          Given I already have USNSCC ID "D322A3A" in the roster.
          When I add a new cadet with USNSCC ID "D322A3A"
          Then I should see "Runtime Execption: USNSCC ID is already taken"