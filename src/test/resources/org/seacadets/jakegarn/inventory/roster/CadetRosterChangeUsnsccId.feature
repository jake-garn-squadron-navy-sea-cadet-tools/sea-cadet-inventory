# new feature
# Tags: optional

Feature: Change cadet USNSCC Id in the roster.

  Scenario Outline: I need to change cadets USNSCC Id.

    Given Cadet USNSCC ID is "<usnsccId>"
    When USNSCC ID needs to change to "<new>"
    Then The new USNSCC Id in the record will be "<new>"

    Examples:
      | new     | usnsccId |
      | D322A3F | A322A3F  |
      | EF1233D | BF1233D  |
      | F32A111 | C32A111  |