# new feature
# Tags: optional

Feature: Change cadet last name in the roster.

  Scenario Outline: I need to rename a cadets last name.

    Given Cadet USNSCC ID is "<usnsccId>"
    When Last name needs to change to "<new>"
    Then The new last name in the record will be "<new>"

    Examples:
      | new     | usnsccId |
      | Smith   | A322A3F  |
      | Smith   | BF1233D  |
      | Richard | C32A111  |