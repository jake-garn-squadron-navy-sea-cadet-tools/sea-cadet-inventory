# new feature
# Tags: optional

Feature: Change cadet first name in the roster.

  Scenario Outline: I need to rename a cadets first name.

    Given Cadet USNSCC ID is "<usnsccId>"
    When First name needs to change to "<new>"
    Then The new first name in the record will be "<new>"

    Examples:
      | new    | usnsccId |
      | Bob    | A322A3F  |
      | Tim    | BF1233D  |
      | MrCool | C32A111  |