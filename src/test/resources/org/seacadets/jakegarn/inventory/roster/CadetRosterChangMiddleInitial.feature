Feature: Change cadet middle initial in the roster.

  Scenario Outline: I need to rename a cadets middle initial.

    Given Cadet USNSCC ID is "<usnsccId>"
    When Middle initial needs to change to "<new>"
    Then The new middle initial in the record will be "<new>"

    Examples:
      | new | usnsccId |
      | S   | A322A3F  |
      | V   | BF1233D  |
      | C   | C32A111  |