package org.seacadets.jakegarn.inventory.roster;

import io.cucumber.java.nl.Stel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RosterRecordTest {
    private RosterRecord record;

    @BeforeEach
    public void setup() {
        record = new RosterRecord("Arthur",
                "Klingenberg",
                "R",
                0x030ACF19);
    }

    @Test
    @DisplayName("Get First Name")
    public void getFirstName() {
        assertEquals("Arthur", record.getFirstName());
    }

    @Test
    @DisplayName("Get Last Name")
    public void getLastName() {
        assertEquals("Klingenberg", record.getLastName());
    }

    @Test
    @DisplayName("Get Middle Initial")
    public void getMiddleInitial(){
        assertEquals("R", record.getMiddleInitial());
    }

    @Test
    @DisplayName("Get USNSCC ID")
    public void getUsnsccId() {
        assertEquals(0x030ACF19, record.getUsnsccId());
    }

    @Test
    @DisplayName("Set First Name")
    void setFirstName() {
        record.setFirstName("David");
        assertEquals("David", record.getFirstName());
    }

    @Test
    @DisplayName("Set Last Name")
    void setLastName() {
        record.setLastName("Smith");
        assertEquals("Smith", record.getLastName());
    }

    @Test
    @DisplayName("Set Middle Initial")
    void setMiddleInitial() {
        record.setMiddleInitial("B");
        assertEquals("B",record.getMiddleInitial());
    }

    @Test
    @DisplayName("Throws MiddleInitialToLong when setting the middle initial, and it exceeds 1 charter.")
    void middleInitialThrowsToLong(){
        assertThrows(RosterRecord.MiddleInitialIsExedesOneChariter.class,
                () -> record.setMiddleInitial("Jr"));
    }

    @Test
    @DisplayName("Throws MiddleInitialToLong when the constructors middle Initial exceeds 1 charter.")
    void constructorMiddleInitialThrowsWhenToLong() {
        assertThrows(RosterRecord.MiddleInitialIsExedesOneChariter.class,
                () -> new RosterRecord("Bob", "Johna", "Ray", 0x34));
    }

    @Test
    @DisplayName("Set USNSCCID")
    void setUsnsccid() {
        record.setUsnsccid(3);
        assertEquals(3, record.getUsnsccId());
    }
}
