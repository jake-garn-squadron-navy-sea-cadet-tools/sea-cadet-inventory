package org.seacadets.jakegarn.inventory.roster;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.*;


public class RosterStepdefs {
    private static Roster roster = new Roster();
    private RosterRecord cadet;
    long usnsccId;

    @Given("New cadet {string}, {string}, {string}, and USNSCC ID {string}")
    public void new_cadets_name_name_initial_and_id(String firstName, String lastName, String middleInitial, String usnsccId) {
        cadet = new RosterRecord(firstName, lastName, middleInitial, Long.parseLong(usnsccId, 16));
    }

    @When("I add a cadet to the roster")
    public void i_add_a_cadet_to_the_roster() {
        roster.add(cadet);
    }

    @Then("I should be told the roster new size is {int}")
    public void i_should_be_told_the_roster_new(Integer size) {
        assertEquals(size, roster.size());
    }

    @Given("Cadet USNSCC ID is {string}")
    public void cadet_USNSCC_ID_is(String usnsccId) {
        this.usnsccId = Long.parseLong(usnsccId, 16);
    }

    @When("First name needs to change to {string}")
    public void name_needs_to_change_to(String first) {
        roster.editFirstName(usnsccId, first);
    }

    @Then("The new first name in the record will be {string}")
    public void the_new_name_in_the_record_will_be(String first) {
        assertEquals(first, roster.get(usnsccId).getFirstName());
    }

    @When("Last name needs to change to {string}")
    public void last_name_needs_to_change_to(String last) {
        roster.editLastName(usnsccId, last);
    }

    @Then("The new last name in the record will be {string}")
    public void the_new_last_name_in_the_record_will_be(String last) {
        assertEquals(last, roster.get(usnsccId).getLastName());
    }

    @When("Middle initial needs to change to {string}")
    public void middle_initial_needs_to_change_to(String middle) {
        roster.editMiddleInitial(usnsccId, middle);
    }

    @Then("The new middle initial in the record will be {string}")
    public void the_new_middle_initial_in_the_record_will_be(String middle) {
        assertEquals(middle, roster.get(usnsccId).getMiddleInitial());
    }

    @When("USNSCC needs to change to {string}")
    public void usnscc_needs_to_change_to(String usnsccId) {
        roster.editUsnsccId(this.usnsccId,Long.parseLong(usnsccId, 16));
    }

    @Then("The new USNSCC Id in the record will be {string}")
    public void the_new_USNSCC_Id_in_the_record_will_be(String usnsccId) {
        long newUsNsccId = Long.parseLong(usnsccId,16);
        assertEquals(newUsNsccId, roster.get(newUsNsccId).getUsnsccId());
    }

}