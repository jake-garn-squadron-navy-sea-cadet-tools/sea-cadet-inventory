package org.seacadets.jakegarn.inventory.roster;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RosterTest {
    private Roster roster = new Roster();
    private String first;
    private String last;
    private String middleIniTial;
    private long usnsccid;

    public RosterTest() {
        first = "Arthur";
        last = "Klingenberg";
        middleIniTial = "R";
        usnsccid = 0x030ACF19;
    }

    @AfterEach
    public void clean(){
        roster.clear();
    }

    @Test
    @DisplayName("Check roster is empty.")
    public void isEmptywithOutClear() {
        roster = new Roster();
        assertTrue(roster.isEmpty());
    }

    @Test
    @DisplayName("Check roster cleared.")
    public void isEmpty() {
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        roster.clear();
        assertTrue(roster.isEmpty());
    }

    @Test
    @DisplayName("Check that roster is not empty.")
    public void isNotEmpty(){
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        assertFalse(roster.isEmpty());
    }

    @Test
    @DisplayName("Add a record.")
    public void add() {
        RosterRecord record = new RosterRecord(first, last, middleIniTial, usnsccid);
        roster.add(record);
        assertEquals(1, roster.size());
    }

    @Test
    @DisplayName("EditFirstName() throws InvalidUsNsccId.")
    public void editFirstNameThrowsInvalidUsNsccid(){
        assertThrows(Roster.InvalidUssccId.class,
                () -> roster.editFirstName(1233232, "David"));
    }

    @Test
    @DisplayName("Change first name given a usnsccId.")
    public void editFirst() {
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        roster.editFirstName(usnsccid, "David");
        assertEquals("David", roster.get(usnsccid).getFirstName());
    }

    @Test
    @DisplayName("EditLastName() throws InvalidUsNsccId")
    public void editLastNameThrowsInvalidUsNsccId() {
        assertThrows(Roster.InvalidUssccId.class,
                () -> roster.editLastName(usnsccid, "Smith"));
    }

    @Test
    @DisplayName("Change last name given a usnsccId")
    public void editLast(){
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        roster.editLastName(usnsccid, "Smith");
        assertEquals("Smith", roster.get(usnsccid).getLastName());
    }

    @Test
    @DisplayName("EditMiddleInitial() throws InvalidUsnsccId")
    public void editMiddleInitialThrowsInvaidUsnsccId(){
        assertThrows(Roster.InvalidUssccId.class,
                ()-> roster.editMiddleInitial(usnsccid, "A"));
    }

    @Test
    @DisplayName("Edit middle initial.")
    public void editMiddleInitial(){
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        roster.editMiddleInitial(usnsccid, "B");
        assertEquals("B", roster.get(usnsccid).getMiddleInitial());
    }

    @Test
    @DisplayName("EditUsnsccId throw InvalidUsnsccid.")
    public void editUsnsccIdThrowsInvalidUsnsccid() {
        assertThrows(Roster.InvalidUssccId.class,
                () -> roster.editUsnsccId(35, 37));
    }

    @Test
    @DisplayName("Change USNSCC Id given usnsccId")
    public void editUsnsccUId(){
        roster.add(new RosterRecord(first, last, middleIniTial, usnsccid));
        roster.editUsnsccId(usnsccid, 0x030ACFA0);
        assertEquals("Arthur", roster.get(0x030ACFA0).getFirstName());
    }

    @Test
    @DisplayName("Remove record")
    public void remove() {
        roster.remove( 0x030ACF19);
        assertEquals(0, roster.size());
    }
}
