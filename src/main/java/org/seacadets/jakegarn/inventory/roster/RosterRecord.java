package org.seacadets.jakegarn.inventory.roster;

public class RosterRecord {


    private String firstName;
    private String lastName;
    private String middleInitial;
    private long usnsccId;

    public RosterRecord(String firstName, String lastName, String middleInitial, long usnsccId) {
        this.firstName = firstName;
        this.lastName = lastName;
        setMiddleInitial(middleInitial);
        this.usnsccId = usnsccId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public long getUsnsccId() {
        return usnsccId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleInitial(String middleInitial) {
        if (middleInitial.length() <= 1)
            this.middleInitial = middleInitial;
        else
            throw new MiddleInitialIsExedesOneChariter();
    }

    public void setUsnsccid(long usnsccId) {
        this.usnsccId = usnsccId;
    }

    /* ******************** Thrown Classes ***************************/

    public static class MiddleInitialIsExedesOneChariter extends RuntimeException {
    }
}
