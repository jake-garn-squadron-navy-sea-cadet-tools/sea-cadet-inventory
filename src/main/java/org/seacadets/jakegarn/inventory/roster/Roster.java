package org.seacadets.jakegarn.inventory.roster;

import java.util.HashMap;
import java.util.Map;

public class Roster {
    Map<Long, RosterRecord> records = new HashMap<>();

    public boolean isEmpty() {
        return records.isEmpty();
    }

    public void add(RosterRecord record) {
        records.put(record.getUsnsccId(), record);
    }

    public void remove(long usnsccId) {
        records.remove(usnsccId);
    }


    public int size() {
        return records.size();
    }


    public void editFirstName(long usnsccid, String firstName) {
        try {
            records.get(usnsccid).setFirstName(firstName);
        }catch (NullPointerException e) {
            throw new InvalidUssccId();
        }
    }

    public RosterRecord get(long usnsccid) {
        return records.get(usnsccid);
    }

    public void clear() {
        records.clear();
    }

    public void editLastName(long usnsccid, String lastName) {
        try {
            records.get(usnsccid).setLastName(lastName);
        } catch (NullPointerException e) {
            throw new InvalidUssccId();
        }
    }

    public void editMiddleInitial(long usnsccid, String middleInitial) {
        try {
            records.get(usnsccid).setMiddleInitial(middleInitial);
        } catch (NullPointerException e) {
            throw new InvalidUssccId();
        }
    }

    public void editUsnsccId(long oldUsnsccId, long newUsnsccId) {
        try {
            RosterRecord r = records.get(oldUsnsccId);
            r.setUsnsccid(newUsnsccId);
            records.remove(oldUsnsccId);
            records.put(newUsnsccId, r);
        } catch (NullPointerException e) {
            throw new InvalidUssccId();
        }
    }

    /* ********************** Throwing Class ********************** */

    public static class InvalidUssccId extends RuntimeException {
    }
}
