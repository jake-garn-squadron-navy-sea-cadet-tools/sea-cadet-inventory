
# Sea Cadet Inventory 
The intent of this project to build a small inventory system to help track uint inventory and who received the equipment.

It will be developed in Java with Gradle as the build system.  The additional tools will include spot bugs for static code checks, Jacoco for code coverage, Junit 5 for unit test, and Java FX for the GUI.  Cucumber.io will be for behavioral driven development.  Cucumber will allow none developers to contribute to the project requirements and define the business rules.
